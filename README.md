

# Quentin : GAN first target architecture 

## Discriminator
Type : CNN.  
Input Layer : 32 * 96 pixels ( Img1 + Img2 + Merged1+2 ).    
Output :  ( Real Merge, False Merge ) : 1 Dense Sigmoid output layer.  
[CTRL + F define_discriminator seems to be good enough](https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/)

## Generator 
Type : CNN or ... ?  
Input Layer : 32 * 64 pixels ( ImgSrc1 + ImgSrc2).    
Output : 32 * 32 img.  
[CTRL + F define_generator need to be adapted ](https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/)

## Grayscale or Colorful ?
If grayscale a pixel is a single value.   
Else we need to make the process for each channel ( 4 times for R, G, B, Alpha in png file).   

## Next Step 

1. Training only a generator w/o adversarial stuff.