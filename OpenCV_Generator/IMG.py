# Quentin Raymondaud
# 72x216 IMG editor
# Free to use

import cv2
import os
import numpy as np
from numpy import dot
from numpy.linalg import norm

## LOAD
## Images from path provided in folder 
## Only Images from index provided in start
## To index provided in end
## if retFn set to true it returns a tuple ( images, filenames )
def load_images_from_folder(folder, start, end, retFn=False):
	images = []
	fn = []
	index = 0
	filenames = os.listdir(folder)
	for filename in filenames:
		img = cv2.imread(os.path.join(folder,filename), cv2.IMREAD_UNCHANGED)
		if img is not None and index < end and index >= start:
			images.append(img)
			fn.append(filename)
		if index == end :
			if ( not retFn ):
				return images 
			else : 
				return images, fn
		index+=1
	if ( not retFn ):
		return images 
	else : 
		return images, fn

# Get a single Emoji from a triple emoji
# img = [ EM0, EM1, EM2] in 96*size img
# Ex: crop_img( img, 1) return EM1 
def vertical_crop(tripleEmoji, emojiIndex, size=32):
	return tripleEmoji[ 0:size, emojiIndex*size:(emojiIndex+1)*size ]

# Merge an Emoji into a triple emoji
# img = [ EM0, EM1, EM2] in 96*72 img
# Ex: merge_img( EM, image, 1) returns [ EM0, EM, EM2]
def merge_img(singleEmoji, tripleEmoji, emojiIndex, size=32 ):
	tripleEmoji[ 0:size, emojiIndex*size:(emojiIndex+1)*size] = singleEmoji
	return tripleEmoji

def merge_img_horizontal(img,imgPart,start,stop):
	img[start:stop, 0:img.shape[0]] = imgPart[start:stop]
	return img



def lateral_crop(img, pt1=(71,0),pt2=(0,71)):
	#cv2.line(img, pt1, pt2, ( 255, 255, 255, 255))
	m = float(pt2[1] - pt1[1])/float(pt2[0] - pt1[0])
	c = pt1[1] - m*pt1[0]
	mask1 = np.zeros(img.shape, np.uint8)
	mask2 = np.zeros(img.shape, np.uint8)
	#for every point in the image
	for x in np.arange(0, 71):
		for y in np.arange(0, 71):
			if y > m*x + c:
				mask2[y][x] = (255, 255, 255, img[y][x][3])
			if y < m*x + c:
				mask1[y][x] = (255, 255, 255, img[y][x][3])

	top = cv2.merge((img[:, :, 0], img[:, :, 1], img[:, :, 2], mask1[:,:, 3]))
	bottom = cv2.merge((img[:, :, 0], img[:, :, 1], img[:, :, 2], mask2[:,:, 3]))
	return top, bottom


def horizontal_crop(img, yValue=36 ):
	top = np.zeros(img.shape, np.uint8)
	bottom = np.zeros(img.shape, np.uint8)
	top[0:yValue][:,] = img[0:yValue][:,]
	bottom[yValue:72][:,] = img[yValue:72][:,]
	return top,bottom



def np_hist_to_cv(np_histogram_output):
	counts, bin_edges = np_histogram_output
	return counts.ravel().astype('float32')


def cosine_similarities(a,b): 
	return dot(a, b)/(norm(a)*norm(b))

# USE HISTOGRAM IN ORDER TO DETERMINE AN HORIZONTAL yVALUE TO CROP
def find_horizontal_limit(img,img2):
	imgColumn = np.zeros( img.shape, dtype = np.uint8)
	imgColumn2 = np.zeros( img.shape, dtype = np.uint8)
	maxVal = -1000
	iMax = -1

	for i in range(30,img.shape[1]-20):
		imgColumn = np.split(img[:,i],2)
		imgColumn2 = np.split(img[:,i],2)
		val = cosine_similarities(imgColumn[0],imgColumn2[0])
		val += cosine_similarities(imgColumn[1],imgColumn2[1])
		if ( val > maxVal ):
			iMax = i
			maxVal = val
	return iMax

# USE HISTOGRAM IN ORDER TO DETERMINE AN HORIZONTAL yVALUE TO CROP
def DEPRACATED_find_horizontal_limit(img):


	######## SEARCH FOR AN HISTOGRAM TO HARDCODE #######

	#histBASE = np_hist_to_cv(np.histogram(img[:,30]))
	#print(histBASE)							

	######## SEARCH FOR AN HISTOGRAM TO HARDCODE #######


	histBASE = np.array([ 10.,  0.,  0.,  0.,  0.,  0., 0., 20., 10., 20.], np.float32)
	imgColumn = np.zeros( img.shape, dtype = np.uint8)
	maxVal = -200
	iMax = -1
	for i in range(20,img.shape[1]-20):
	    #imgColumn = np.delete( imgColumn, i, 1)
        #ret3,th3 = cv2.threshold(img,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        #cv2.imshow("th",th3)
	    imgColumn = img[:,i]
	    histLine = np_hist_to_cv(np.histogram(imgColumn))
	    val = cv2.compareHist(histLine, histBASE, cv2.HISTCMP_CHISQR)
	    if ( val > maxVal ):
	    	iMax = i
	    	maxVal = val
	return iMax




